#ifndef MEAN_MOLECULAR_WEIGHT_H
#define MEAN_MOLECULAR_WEIGHT_H

#include "constants.h"
#include <grackle.h>

/*********************************************************************
/ Simple function to estimate the mean molecular weight
*********************************************************************/

/* Get mean molecular weight given species densities */
gr_float mean_weight_from_densities(gr_float density, gr_float nHI,
                                    gr_float nHII, gr_float nHeI,
                                    gr_float nHeII, gr_float nHeIII,
                                    gr_float ne, gr_float mass_units) {

  gr_float n;
  gr_float mu;

  n = nHI + nHII + nHeI + nHeII + nHeIII + ne;
  mu = density / n / (mh / mass_units);

  return mu;
}

/*********************************************************************
/ Compute the mean molecular weight using the same rules than Grackle
*********************************************************************/

#define MU_METAL 16.0

int mean_weight_local_like_grackle(chemistry_data *my_chemistry,
                                   grackle_field_data *my_fields,
                                   gr_float *mu) {

  gr_float number_density = 0.;
  gr_float inv_metal_mol = 1.0 / MU_METAL;
  int i, dim, size = 1;

  for (dim = 0; dim < my_fields->grid_rank; dim++)
    size *= my_fields->grid_dimension[dim];

  if (!my_chemistry->use_grackle)
    return SUCCESS;

  for (i = 0; i < size; i++) {

    if (my_chemistry->primordial_chemistry > 0) {
      number_density =
          0.25 * (my_fields->HeI_density[i] + my_fields->HeII_density[i] +
                  my_fields->HeIII_density[i]) +
          my_fields->HI_density[i] + my_fields->HII_density[i] +
          my_fields->e_density[i];
    }

    /* Add in H2. */

    if (my_chemistry->primordial_chemistry > 1) {
      number_density +=
          my_fields->HM_density[i] +
          0.5 * (my_fields->H2I_density[i] + my_fields->H2II_density[i]);
    }

    if (my_fields->metal_density != NULL) {
      number_density += my_fields->metal_density[i] * inv_metal_mol;
    }

    /* Ignore deuterium. */

    mu[i] = my_fields->density[0] / number_density;
  }

  return SUCCESS;
}

/* #define NT 10 */
/*  */
/* gr_float tt[NT] = {1.0e+01, 1.0e+02, 1.0e+03, 1.0e+04, 1.3e+04, */
/*                  2.1e+04, 3.4e+04, 6.3e+04, 1.0e+05, 1.0e+09}; */
/* gr_float mt[NT] = {1.18701555, 1.15484424, 1.09603514, 0.9981496, 0.96346395,
 */
/*                  0.65175895, 0.6142901,  0.6056833,  0.5897776, 0.58822635};
 */
/* gr_float unr[NT]; */
/*  */
/* [> mean molecular weight as a function of the Temperature <] */
/* gr_float mean_weight_from_temperature(gr_float T) { */
/*  */
/*   gr_float logt; */
/*   gr_float ttt; */
/*   gr_float slope; */
/*   gr_float mu; */
/*  */
/*   int j; */
/*  */
/*   logt = log(T); */
/*   ttt = exp(logt); */
/*  */
/*   if (ttt < tt[0]) */
/*     j = 1; */
/*   else */
/*     for (j = 1; j < NT; j++) */
/*       if ((ttt > tt[j - 1]) && (ttt <= tt[j])) */
/*         break; */
/*  */
/*   slope = log(mt[j] / mt[j - 1]) / log(tt[j] / tt[j - 1]); */
/*   mu = exp(slope * (logt - log(tt[j])) + log(mt[j])); */
/*  */
/*   return mu; */
/* } */

#endif /* MEAN_MOLECULAR_WEIGHT_H */
