/* (Physical) constants. */

#ifndef CONSTANTS_H
#define CONSTANTS_H

/* Physical
 * --------------- */

/* Hydrogen mass in g */
#define mh 1.67262171e-24
/* erg/K */
#define kboltz 1.3806504e-16

/* Code internals
 * --------------- */
#define SUCCESS 1
#define TINY_NUMBER 1.0e-20

#endif /* define CONSTANTS_H */
