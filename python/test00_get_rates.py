#!/usr/bin/env python3

from scipy import integrate
import numpy as np
import Ptools as pt
#from pyrates import cross_secs
#from pyrates.elements import *





def SigmaRAMSES_RT(E):
  # taken from RT_spectra.f90
  # E is in eV
  # only for HI->HII
  
  E0 = 4.298e-1
  cs0 = 5.475e-14
  P  = 2.963
  ya = 32.88
  yw  = 0
  y0 = 0
  y1 = 0

  x = E/E0 - y0
  y = np.sqrt(x**2+y1**2)
  
  return cs0 * ((x-1.)**2 + yw**2) * y**(0.5*P-5.5)/(1.+np.sqrt(y/ya))**P
  



# values from Draine's book :
h   = 6.62607e-27       # erg s, Planck constant
k_B = 1.3806505e-23     # J K-1, Boltzmann constant
c   = 2.99792458e10     # cm s-1, speed of light

erg_per_eV = 1.60218e-12  # erg
J_per_erg  = 1.0e-7       # joules

k_B_in_erg = k_B / J_per_erg  # conversion to erg K-1


E_ion  = 13.6 *erg_per_eV # eV to erg


nu_ion = E_ion/h     # in Hz
nu_inf   = 1.0e25  # in Hz, mimics 'infinity' in integration over frequencies


# Photon flux
Flux = 1e12 # photon/cm3/s


def do_integration(integrand, integ_range, log_mode):
    """Integrate the given integrand along the given range.
    Use scipy.integrate.trapz function.
    Set log_mode to True for more accurate result,
    but only if provided arrays contains stricly positive values.

    :param integrand: Array to integrate.
    :type integrand: ndarray.
    :param integ_range: Array of values along which integration is performed.
    :type integ_range: ndarray.
    :param log_mode: If True, use log to integrate (see code).
    :type log_mode: bool.
    :returns: Result of integration.
    :rtype: float.
    """
    f = integrand
    x = integ_range
    if log_mode:
        with np.errstate(divide='ignore'):
            log_x = np.log(x)
            log_f = np.log(f)
            explog_int = np.exp(log_x + log_f)
            return integrate.trapz(explog_int, log_x)
    else:
        return integrate.trapz(f, x)




def get_black_body(T,frequency_range):
    """Black body from Planck's law  (return a  specific intensity [erg s-1 cm-2 Hz-1 sr-1]).
    This is equivalent to : B_nu


    :param temperature: Black body effective temperature, in K.
    :type temperature: float.
    :param luminosity: Black body luminosity, in erg s-1.
    :type luminosity: float.
    :returns: Intensity, in units erg s-1 cm-2 Hz-1.
    :rtype: ndarray or float.

    """
    nu = frequency_range  # Hz
    hnu = h * nu           # erg
    kbt = k_B_in_erg * T   # erg
    with np.errstate(over='ignore'):
        B_nu = 2.0 * h * nu**3 / c**2 / (np.exp(hnu / kbt) - 1.0)  # erg s-1 cm-2 Hz-1 sr-1
    return (nu, B_nu)




Npoints = int(1e4)
frequency_range = np.logspace(np.log10(nu_ion),np.log10(nu_inf), Npoints)

#F = 1e12  # photons per 1/cm2 1/s
T = 1e5   # BB Temperature

nu,B_nu = get_black_body(T,frequency_range)



################################################
# photon flux

# photon flux per frequency     [1/cm2 1/s 1/Hz 1/Sr]
integrand = B_nu/(h*nu)
# photon flux                   [1/cm2 1/s  1/Sr]
F = do_integration(integrand, nu, log_mode=True)

################################################
# mean energy

# energy per frequency
integrand = B_nu
# energy
E = do_integration(integrand, nu, log_mode=True)

E_mean = E/F  

print("E_ion (H ionization energy)          = %g eV"%(E_ion/erg_per_eV))
print("E_mean (mean energy per photon)      = %g eV"%(E_mean/erg_per_eV))                           


################################################
# Averaged photo-ionization cross section



# photoelectric cross section, cm2
element = 'HeI'
#sigma = cross_secs.photoelectric_cross_section(element, nu)
sigma = SigmaRAMSES_RT(nu*h/erg_per_eV)
J_nu = B_nu

integrand = B_nu/(h*nu) * sigma
I1 = do_integration(integrand, nu, log_mode=True)

integrand = B_nu/(h*nu) 
I2 = do_integration(integrand, nu, log_mode=True)

Sigma_N = I1/I2     # cm2

print("Sigma_N (cross section)              = %g cm2"%Sigma_N)                             



################################################
# Energy weighted photo-ionization cross section


# photoelectric cross section, cm2
element = 'HeI'
#sigma = cross_secs.photoelectric_cross_section(element, nu)
sigma = SigmaRAMSES_RT(nu*h/erg_per_eV)
J_nu = B_nu

integrand = B_nu * sigma
I1 = do_integration(integrand, nu, log_mode=True)

integrand = B_nu
I2 = do_integration(integrand, nu, log_mode=True)

Sigma_E = I1/I2     # cm2

print("Sigma_E (cross section egy weighted) = %g cm2"%Sigma_E)      



################################################
# Ionization rate

Gamma = Flux * Sigma_N

print("Gamma (Ionization rate)              = %g 1/s"%Gamma)      



################################################
# Photo heating rate

Epsilon = Flux * (Sigma_E * E_mean - Sigma_N * E_ion)

print("Epsilon (Photoheating rate)          = %g erg/s"%Epsilon)    












