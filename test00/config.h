/* Some global definitions and configurations. */
/* NOTE: Don't put this in the include directory,
 * as we'll have to modify this for each example. */

#ifndef CONFIG_H
#define CONFIG_H

#define FIELD_SIZE 1
#define griddim 1

#define localHydrogenFractionByMass 1.0
/* 0.76       [> should be the same value as defined in Grackle <] */

#endif /* define CONFIG_H */
