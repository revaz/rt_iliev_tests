
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <grackle.h>

#include "config.h"
#include "constants.h"
#include "ionization_equilibrium.h"
#include "mean_molecular_weight.h"

/* use gr_float to use the same type of floats
 * that grackle is compiled in. Compile grackle
 * with precision-32 if you want floats, or
 * precision-64 otherwise. */
gr_float HI_density;
gr_float HII_density;
gr_float HeI_density;
gr_float HeII_density;
gr_float HeIII_density;
gr_float e_density;

gr_float nH;
gr_float nHI;
gr_float nHII;
gr_float nHeI;
gr_float nHeII;
gr_float nHeIII;
gr_float ne;

double density_units;
double length_units;
double time_units;
double mass_units;
double mean_weight;

gr_float *temperature;
gr_float *mu;

FILE *fd;

/*********************************************************************
/ Main
*********************************************************************/

int main() {

  double t = 0.;
  double dt = 0.;
  double logt = 0.;
  double logdt = 0.;

  /* define units */
  density_units = 1.67262171e-24;
  length_units = 1.0;
  time_units = 1.0e12;
  mass_units = density_units * length_units * length_units * length_units;

  /* define main parameters */
  double T = 1e2;     /* initial temperature in K */
  double Density = 1; /* atom (mh) per cc */

  int UVbackground = 0;         /* toogle on/off the UV background */
  int primordial_chemistry = 1; /* choose the chemical network */

  double HydrogenFractionByMass = localHydrogenFractionByMass;

  int use_radiative_transfer = 1;
  double RT_HI_ionization_rate = 1.63021e-06; /* in s^-1 */
  double RT_HeI_ionization_rate = 0;
  double RT_HeII_ionization_rate = 0;
  double RT_H2_dissociation_rate = 0;
  double RT_heating_rate = 1.65142e-17; /* in erg/s/cm3 */

  double RT_tend = 0.5e6;                  /* yr */
  RT_tend = RT_tend * 3.15e7 / time_units; /* yr to code unit */

  /* time integration */
  int n = 100000;         /* number of steps */
  double tinit = 1e-6;    /* yr */
  double tend = 1e8;      /* yr */
  int logIntegration = 1; /* log Integration */

  if (logIntegration) {

    logdt = (log10(tend) - log10(tinit)) / n;
    logt = log10(tinit) + log10(3.15e7 / time_units);

    t = tinit * 3.15e7 / time_units;   /* yr to code units */
    tend = tend * 3.15e7 / time_units; /* yr to code units */

    /* tinit = pow(10,log10(tend)/n); */
  } else {
    dt = (tend - tinit) / n;

    dt = dt * 3.15e7 / time_units;     /* yr to code units */
    t = tinit * 3.15e7 / time_units;   /* yr to code units */
    tend = tend * 3.15e7 / time_units; /* yr to code units */
  }

  /* convert to code units */
  Density = Density * mh / density_units; /* atom per cc to code units */

  /* define the hydrogen number density */
  nH = HydrogenFractionByMass * Density / (mh / mass_units);

  /* calculate densities of primordial spicies assuming ionization equilibrium
   */
  ionization_equilibrium_calculate_densities(
      T, nH, HydrogenFractionByMass, &nHI, &nHII, &nHeI, &nHeII, &nHeIII, &ne);

  /* check calculate_densities */
  /*
  for (ti=1;ti<1000;ti++)
    {
      T = pow(ti/109.,10);
      calculate_densities(T,nH,HydrogenFractionByMass,&nHI,&nHII,&nHeI,&nHeII,&nHeIII,&ne);
      printf("%g %g %g     %g %g %g     %g\n",T,nHI,nHII,nHeI,nHeII,nHeIII,ne);
    }
  exit(-1);
  */

  printf("%15s%15s%15s%15s%15s%15s%15s\n", "Temperature", "nHI", "nHII", "nHeI",
         "nHeII", "nHeIII", "ne");
  printf("%15g%15g%15g%15g%15g%15g%15g\n\n", T, nHI, nHII, nHeI, nHeII, nHeIII,
         ne);

  /* estimate the mean weight based on the temperature */
  /* mean_weight = mean_weight_from_temperature(T); */

  /* estimate the mean weight based on the densities */
  /* = mass density / sum(density of each species) */
  mean_weight = mean_weight_from_densities(Density, nHI, nHII, nHeI, nHeII,
                                           nHeIII, ne, mass_units);

  /* compute the initial mass densities [cgs to code unit] */
  HI_density = nHI * (mh / mass_units);
  HII_density = nHII * (mh / mass_units);

  HeI_density = nHeI * (4 * mh / mass_units);
  HeII_density = nHeII * (4 * mh / mass_units);
  HeIII_density = nHeIII * (4 * mh / mass_units);

  /* !! this is the convention adopted by Grackle
   * !! e_density is the electron density * nh/ne */
  e_density = ne * (mh / mass_units);

  /* output file */
  fd = fopen("out.dat", "w");

  /*********************************************************************
  / Initial setup of units and chemistry objects.
  / This should be done at simulation start.
  *********************************************************************/

  /*********************************************************************
  / Units
  *********************************************************************/

  /* First, set up the units system. We assume cgs
   * These are conversions from code units to cgs. */
  code_units grackle_units_data;
  grackle_units_data.comoving_coordinates =
      0; /* 1 if cosmological sim, 0 if not */
  grackle_units_data.density_units = density_units;
  grackle_units_data.length_units = length_units;
  grackle_units_data.time_units = time_units;
  grackle_units_data.velocity_units =
      grackle_units_data.length_units / grackle_units_data.time_units;
  grackle_units_data.a_units = 1.0; /* units for the expansion factor */
  /* Set expansion factor to 1 for non-cosmological simulation. */
  grackle_units_data.a_value = 1.;

  /* set temperature units */
  double temperature_units =
      mh / kboltz *
      pow(grackle_units_data.a_units * grackle_units_data.length_units /
              grackle_units_data.time_units,
          2);

  /*********************************************************************
  / Chemistry Parameters
  *********************************************************************/

  /* Second, create a chemistry object for parameters.  This needs to be a
   * pointer. */

  /* chemistry_data *grackle_chemistry_data; */
  /* grackle_chemistry_data = malloc(sizeof(chemistry_data)); */
  /* if (set_default_chemistry_parameters(grackle_chemistry_data) == 0) { */
  /*   fprintf(stderr, "Error in set_default_chemistry_parameters.\n"); */
  /*   return EXIT_FAILURE; */
  /* } */
  chemistry_data grackle_chemistry_data = _set_default_chemistry_parameters();

  /* Set parameter values for chemistry.
   * Access the parameter storage with the struct you've created
   * or with the grackle_data pointer declared in grackle.h (see further below).
   */
  grackle_chemistry_data.use_grackle = 1;            /* chemistry on */
  grackle_chemistry_data.with_radiative_cooling = 1; /* cooling on */
  grackle_chemistry_data.primordial_chemistry =
      primordial_chemistry; /* molecular network */
  /* grackle_chemistry_data->dust_chemistry = 1;         [> dust processes <] */
  grackle_chemistry_data.metal_cooling = 0;           /* metal cooling on */
  grackle_chemistry_data.UVbackground = UVbackground; /* UV background on */
  grackle_chemistry_data.grackle_data_file = "CloudyData_UVB=HM2012.h5"; /* data file */
  grackle_chemistry_data.use_radiative_transfer = use_radiative_transfer;
  grackle_chemistry_data.HydrogenFractionByMass = HydrogenFractionByMass;

  /* Finally, initialize the chemistry object. */
  /* if (initialize_chemistry_data(&grackle_units_data) == 0) { */
  /*   fprintf(stderr, "Error in initialize_chemistry_data.\n"); */
  /*   return EXIT_FAILURE; */
  /* } */

  /* Initialize the chemistry_data_storage object to be
   * able to use local functions */
  chemistry_data_storage *grackle_chemistry_rates;
  grackle_chemistry_rates = malloc(sizeof(chemistry_data_storage));
  if (_initialize_chemistry_data(&grackle_chemistry_data, grackle_chemistry_rates, &grackle_units_data) == 0) {
    fprintf(stderr, "Error in initialize_chemistry_data.\n");
    return EXIT_FAILURE;
  }

  /*********************************************************************
  / Initial values
  *********************************************************************/

  /* Create struct for storing grackle field data */
  grackle_field_data grackle_fields;

  /* Set grid dimension and size.
   * grid_start and grid_end are used to ignore ghost zones. */
  grackle_fields.grid_rank = griddim;
  grackle_fields.grid_dimension =
      malloc(grackle_fields.grid_rank * sizeof(int));
  grackle_fields.grid_start = malloc(grackle_fields.grid_rank * sizeof(int));
  grackle_fields.grid_end = malloc(grackle_fields.grid_rank * sizeof(int));
  /* grackle_fields.grid_dx = 0.0; // used only for H2 self-shielding
   * approximation */

  int i;
  for (i = 0; i < 1; i++) {
    grackle_fields.grid_dimension[i] =
        1; /* the active dimension not including ghost zones. */
    grackle_fields.grid_start[i] = 0;
    grackle_fields.grid_end[i] = 0;
  }
  grackle_fields.grid_dimension[0] = FIELD_SIZE;
  grackle_fields.grid_end[0] = FIELD_SIZE - 1;

  /* Set initial quantities */
  grackle_fields.density = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.internal_energy = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.x_velocity = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.y_velocity = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.z_velocity = malloc(FIELD_SIZE * sizeof(gr_float));
  /* for primordial_chemistry >= 1 */
  grackle_fields.HI_density = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.HII_density = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.HeI_density = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.HeII_density = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.HeIII_density = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.e_density = malloc(FIELD_SIZE * sizeof(gr_float));
  /* for primordial_chemistry >= 2 */
  grackle_fields.HM_density = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.H2I_density = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.H2II_density = malloc(FIELD_SIZE * sizeof(gr_float));
  /* for primordial_chemistry >= 3 */
  grackle_fields.DI_density = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.DII_density = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.HDI_density = malloc(FIELD_SIZE * sizeof(gr_float));
  /* for metal_cooling = 1 */
  grackle_fields.metal_density = malloc(FIELD_SIZE * sizeof(gr_float));

  /* volumetric heating rate (provide in units [erg s^-1 cm^-3]) */
  grackle_fields.volumetric_heating_rate =
      malloc(FIELD_SIZE * sizeof(gr_float));
  /* specific heating rate (provide in units [egs s^-1 g^-1] */
  grackle_fields.specific_heating_rate = malloc(FIELD_SIZE * sizeof(gr_float));

  /* radiative transfer ionization / dissociation rate fields (provide in units
   * [1/s]) */
  grackle_fields.RT_HI_ionization_rate = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.RT_HeI_ionization_rate = malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.RT_HeII_ionization_rate =
      malloc(FIELD_SIZE * sizeof(gr_float));
  grackle_fields.RT_H2_dissociation_rate =
      malloc(FIELD_SIZE * sizeof(gr_float));
  /* radiative transfer heating rate field (provide in units [erg s^-1 cm^-3])
   */
  grackle_fields.RT_heating_rate = malloc(FIELD_SIZE * sizeof(gr_float));

  temperature = malloc(FIELD_SIZE * sizeof(gr_float));
  mu = malloc(FIELD_SIZE * sizeof(gr_float));

  for (i = 0; i < FIELD_SIZE; i++) {

    /* initial density */
    grackle_fields.density[i] = Density;

    /* initial internal energy using initial temperature */
    grackle_fields.internal_energy[i] =
        T / (grackle_chemistry_data.Gamma - 1.0) / mean_weight / temperature_units;

    grackle_fields.HI_density[i] = HI_density;
    grackle_fields.HII_density[i] = HII_density;
    grackle_fields.HeI_density[i] = HeI_density;
    grackle_fields.HeII_density[i] = HeII_density;
    grackle_fields.HeIII_density[i] = HeIII_density;
    grackle_fields.e_density[i] = e_density; /* electron density*mp/me */
    /* grackle_fields.e_density[i]      = TINY_NUMBER *
     * grackle_fields.density[i]; */

    grackle_fields.HM_density[i] = TINY_NUMBER * grackle_fields.density[i];
    grackle_fields.H2I_density[i] = TINY_NUMBER * grackle_fields.density[i];
    grackle_fields.H2II_density[i] = TINY_NUMBER * grackle_fields.density[i];
    grackle_fields.DI_density[i] = TINY_NUMBER * grackle_fields.density[i];
    grackle_fields.DII_density[i] = TINY_NUMBER * grackle_fields.density[i];
    grackle_fields.HDI_density[i] = TINY_NUMBER * grackle_fields.density[i];

    /* solar metallicity */
    grackle_fields.metal_density[i] =
        grackle_chemistry_data.SolarMetalFractionByMass * grackle_fields.density[i];

    grackle_fields.x_velocity[i] = 0.0;
    grackle_fields.y_velocity[i] = 0.0;
    grackle_fields.z_velocity[i] = 0.0;

    grackle_fields.volumetric_heating_rate[i] = 0.0;
    grackle_fields.specific_heating_rate[i] = 0.0;

    grackle_fields.RT_HI_ionization_rate[i] =
        RT_HI_ionization_rate * time_units; /* 1/time_units */
    grackle_fields.RT_HeI_ionization_rate[i] = RT_HeI_ionization_rate;
    grackle_fields.RT_HeII_ionization_rate[i] = RT_HeII_ionization_rate;
    grackle_fields.RT_H2_dissociation_rate[i] = RT_H2_dissociation_rate;
    grackle_fields.RT_heating_rate[i] =
        RT_heating_rate; /* must be checked !!! */
    /* TODO MLADEN: what does 'must be checked' mean here? */
  }

  printf("%15s%15s%15s%15s%15s%15s%15s%15s%15s%15s%15s\n", "Time", "dt",
         "Temperature", "Mean Mol. W.", "Tot dens.", "HI dens.", "HII dens.",
         "HeI dens.", "HeII dens.", "HeIII dens.", "e- num. dens.");
  printf(
      "%15.3e%15.3e%15.1f%15.3f%15.3e%15.3e%15.3e%15.3e%15.3e%15.3e%15.3e \n",
      t / 3.15e7 * time_units, dt / 3.15e7 * time_units, T, mean_weight,
      grackle_fields.density[0], grackle_fields.HI_density[0],
      grackle_fields.HII_density[0], grackle_fields.HeI_density[0],
      grackle_fields.HeII_density[0], grackle_fields.HeIII_density[0],
      grackle_fields.e_density[0]);

  /*********************************************************************
  / Calling the chemistry solver
  / These routines can now be called during the simulation.
  *********************************************************************/

  while (t < tend) {

    /* increment time */
    if (logIntegration) {
      dt = pow(10, logt + logdt) - pow(10, logt);
      logt = logt + logdt;
      t = pow(10, logt);

      /* dt = t*tinit - t; */
      /* t  = t*tinit; */
    } else
      t = t + dt;

    if (t > RT_tend) {
      grackle_chemistry_data.use_radiative_transfer = 0;
      grackle_fields.RT_HI_ionization_rate[0] = 0;
      grackle_fields.RT_HeI_ionization_rate[0] = 0;
      grackle_fields.RT_HeII_ionization_rate[0] = 0;
      grackle_fields.RT_H2_dissociation_rate[0] = 0;
      grackle_fields.RT_heating_rate[0] = 0;
    }

    if (local_solve_chemistry(&grackle_chemistry_data, grackle_chemistry_rates, &grackle_units_data, &grackle_fields, dt) == 0) {
      fprintf(stderr, "Error in solve_chemistry.\n");
      return EXIT_FAILURE;
    }

    /* Calculate temperature. */
    if (local_calculate_temperature(&grackle_chemistry_data, grackle_chemistry_rates, &grackle_units_data, &grackle_fields, temperature) == 0) {
      fprintf(stderr, "Error in calculate_temperature.\n");
      return EXIT_FAILURE;
    }

    /* Calculate mean weight. */
    /* if (calculate_mean_weight(&my_chemistry, &my_rates,
     * &grackle_units_data,&grackle_fields, */
    /* mu) == 0) { */
    if (mean_weight_local_like_grackle(&grackle_chemistry_data, &grackle_fields, mu) !=
        SUCCESS) {
      fprintf(stderr, "Error in local_calculate_mean_weight.\n");
      return EXIT_FAILURE;
    }

    /* [> all variables below are converted in part per cc units <] */
    /* nHI = grackle_fields.HI_density[0]
     * *grackle_units_data.density_units/(mh/mass_units); */
    /* nHII = grackle_fields.HII_density[0]
     * *grackle_units_data.density_units/(mh/mass_units); */
    /* nHeI = grackle_fields.HeI_density[0]
     * *grackle_units_data.density_units/(4*mh/mass_units); */
    /* nHeII = grackle_fields.HeII_density[0]
     * *grackle_units_data.density_units/(4*mh/mass_units); */
    /* nHeIII = grackle_fields.HeIII_density[0]
     * *grackle_units_data.density_units/(4*mh/mass_units); */
    /* ne = grackle_fields.e_density[0]
     * *grackle_units_data.density_units/(mh/mass_units); */
    /* [> ne = nHII + nHeII + 2*nHeIII;     [> e- per cc, this must be equal to
     * <] <] */
    /* grackle_fields.e_density[0] expressed in e- per cc */
    /* [> printf("%g      %g\n", ne , grackle_fields.e_density[0]); <] */

    /* print results */
    printf(
        "%15.3e%15.3e%15.1f%15.3f%15.3e%15.3e%15.3e%15.3e%15.3e%15.3e%15.3e \n",
        t / 3.15e7 * time_units, dt / 3.15e7 * time_units, temperature[0],
        mu[0], grackle_fields.density[0], grackle_fields.HI_density[0],
        grackle_fields.HII_density[0], grackle_fields.HeI_density[0],
        grackle_fields.HeII_density[0], grackle_fields.HeIII_density[0],
        grackle_fields.e_density[0]);

    fprintf(
        fd,
        "%15.3e%15.3e%15.1f%15.3f%15.3e%15.3e%15.3e%15.3e%15.3e%15.3e%15.3e \n",
        t / 3.15e7 * time_units, dt / 3.15e7 * time_units, temperature[0],
        mu[0], grackle_fields.density[0], grackle_fields.HI_density[0],
        grackle_fields.HII_density[0], grackle_fields.HeI_density[0],
        grackle_fields.HeII_density[0], grackle_fields.HeIII_density[0],
        grackle_fields.e_density[0]);
  }

  fclose(fd);

  return EXIT_SUCCESS;
}
