#!/usr/bin/env python3

from scipy import integrate
import numpy as np
import Ptools as pt
from pyrates import cross_secs
from pyrates.elements import *



# values from Draine's book :
h = 6.62607e-27       # erg s, Planck constant
k_B = 1.3806505e-23   # J K-1, Boltzmann constant
c = 2.99792458e10     # cm s-1, speed of light

erg_per_eV = 1.60218e-12  # erg
J_per_erg = 1.0e-7      # joules


k_B_in_erg = k_B / J_per_erg  # conversion to erg K-1
nu_start = 1.0e10  # in Hz
nu_inf = 1.0e25  # in Hz, mimics 'infinity' in integration over frequencies


def do_integration(integrand, integ_range, log_mode):
    """Integrate the given integrand along the given range.
    Use scipy.integrate.trapz function.
    Set log_mode to True for more accurate result,
    but only if provided arrays contains stricly positive values.

    :param integrand: Array to integrate.
    :type integrand: ndarray.
    :param integ_range: Array of values along which integration is performed.
    :type integ_range: ndarray.
    :param log_mode: If True, use log to integrate (see code).
    :type log_mode: bool.
    :returns: Result of integration.
    :rtype: float.
    """
    f = integrand
    x = integ_range
    if log_mode:
        with np.errstate(divide='ignore'):
            log_x = np.log(x)
            log_f = np.log(f)
            explog_int = np.exp(log_x + log_f)
            return integrate.trapz(explog_int, log_x)
    else:
        return integrate.trapz(f, x)




def get_black_body(T,frequency_range):
    """Black body from Planck's law  (return a  specific intensity [erg s-1 cm-2 Hz-1 sr-1]).
    This is equivalent to : B_nu


    :param temperature: Black body effective temperature, in K.
    :type temperature: float.
    :param luminosity: Black body luminosity, in erg s-1.
    :type luminosity: float.
    :returns: Intensity, in units erg s-1 cm-2 Hz-1.
    :rtype: ndarray or float.

    """
    nu = frequency_range  # Hz
    hnu = h * nu           # erg
    kbt = k_B_in_erg * T   # erg
    with np.errstate(over='ignore'):
        B_nu = 2.0 * h * nu**3 / c**2 / (np.exp(hnu / kbt) - 1.0)  # erg s-1 cm-2 Hz-1 sr-1
    return (nu, B_nu)




Npoints = int(1e4)
frequency_range = np.logspace(np.log10(nu_start),np.log10(nu_inf), Npoints)

F = 1e12  # photons per 1/cm2 1/s
T = 1e5   # BB Temperature

nu,B_nu = get_black_body(T,frequency_range)

'''
pt.plot(nu,Bnu)
pt.semilogx()
pt.show()
'''

nu_rge = nu

# compute the number of photons
integrand = np.pi*B_nu/(h*nu)

# number of photons per 1/cm2 1/s
cte = do_integration(integrand, nu, log_mode=True)
#print(cte)

# from Boltzman law
#print(1.5205e+11  * T**3)
#exit()

# normalisation
cte = F/cte
B_nu = cte*B_nu



################################################
# now we can compute
################################################



element = 'HeI'
I_H = elements[element][1]  # eV
nu_min = I_H * erg_per_eV / h  # Hz
    
    
# compute the number of photons
integrand = np.pi*B_nu/(h*nu)


# number of photons per 1/cm2 1/s
# just to check
F1 = do_integration(integrand, nu, log_mode=True)

if ( (F1-F)/F1  > 1e-3 ):
  print("F1=%g is different than F2=%g"%(F1,F))
  exit()

# photoelectric cross section, cm2
sigma = cross_secs.photoelectric_cross_section(element, nu_rge)
J_nu = B_nu


# photo ionization rate
integrand = 4.0 * np.pi * sigma * J_nu / (h * nu_rge)

photo_ionization_rate = do_integration(integrand, nu_rge, log_mode=True)



# heating rate
integrand = 4.0 * np.pi * sigma * J_nu / (h * nu_rge) * (h * nu_rge - h * nu_min)

photo_heating_rate = do_integration(integrand, nu_rge, log_mode=True)


print(photo_ionization_rate)
print(photo_heating_rate)










