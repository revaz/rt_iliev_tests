#!/usr/bin/env python3



from pyrates import mlt
from pyrates.UVbackgrounds import AstroUVbackgrounds
from pyrates.rates import photoionization_rate
from pyrates.rates import photoheating_rate
from pyrates.rates import photodissociation_rate

from pyrates.constants import cm_per_pc, L_sun_in_erg, M_sun


import numpy as np
import Ptools as pt


UVbgs = AstroUVbackgrounds(init_all=False, Npoints=1000)

dist_range = np.linspace(0.1, 100.0, 100) # in pc
mass =1 # M_sun


piHI_rates = np.zeros(dist_range.shape)
piHeI_rates = np.zeros(dist_range.shape)
piHeII_rates = np.zeros(dist_range.shape)

phHI_rates = np.zeros(dist_range.shape)
phHeI_rates = np.zeros(dist_range.shape)
phHeII_rates = np.zeros(dist_range.shape)

pdH2I_rates = np.zeros(dist_range.shape)


(BB_L, BB_T) = mlt.get_luminosity_and_temperature(mass,LT_mode=0)

#BB_T = 1e5
#BB_L = 1.


for i, D in enumerate(dist_range):
  piHI_rates[i]     = photoionization_rate('HI', UVbgs, 'BB', BB_temperature=BB_T, BB_luminosity=BB_L*L_sun_in_erg,BB_distance=D*cm_per_pc)
  piHeI_rates[i]    = photoionization_rate('HeI', UVbgs, 'BB', BB_temperature=BB_T, BB_luminosity=BB_L*L_sun_in_erg,BB_distance=D*cm_per_pc)
  piHeII_rates[i]   = photoionization_rate('HeII', UVbgs, 'BB', BB_temperature=BB_T, BB_luminosity=BB_L*L_sun_in_erg,BB_distance=D*cm_per_pc)

  phHI_rates[i]     = photoheating_rate('HI', UVbgs, 'BB', BB_temperature=BB_T, BB_luminosity=BB_L*L_sun_in_erg,BB_distance=D*cm_per_pc)
  phHeI_rates[i]    = photoheating_rate('HeI', UVbgs, 'BB', BB_temperature=BB_T, BB_luminosity=BB_L*L_sun_in_erg,BB_distance=D*cm_per_pc)
  phHeII_rates[i]   = photoheating_rate('HeII', UVbgs, 'BB', BB_temperature=BB_T, BB_luminosity=BB_L*L_sun_in_erg,BB_distance=D*cm_per_pc)



print(piHI_rates[0])
print(phHI_rates[0])


pt.figure()

pt.plot(dist_range,piHI_rates,c="r")
#pt.plot(dist_range,piHeI_rates,c="g")
#pt.plot(dist_range,piHeII_rates,c="b")
pt.loglog()
pt.xlabel(r"$\rm{distance}\,[\rm{pc}]$")
pt.ylabel(r"$\zeta_i\,[s^{-1}]$")
pt.show()



pt.figure()
pt.plot(dist_range,phHI_rates,c="r")
pt.loglog()
pt.xlabel(r"$\rm{distance}\,[\rm{pc}]$")
pt.ylabel(r"$\Gamma_i\,[erg s^{-1}]$")
pt.show()
