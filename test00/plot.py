#!/usr/bin/env python3


import numpy as np
import Ptools as pt


plotkwargs = {"alpha": 0.4}

mh = 1.67262171e-24  # Hydrogen mass in g

density_units = 1.67e-24
length_units = 1.0
time_units = 1.0e12
# mass_units = density_units*pow(length_units,3);
mass_units = 1.0  # to get atom per cc for n

data = np.loadtxt("out.dat")

Time = data[:, 0]
dt = data[:, 1]
Temperature = data[:, 2]
mu = data[:, 3]

tot_density = data[:, 4]  # mass density

HI_density = data[:, 5]
HII_density = data[:, 6]

HeI_density = data[:, 7]
HeII_density = data[:, 8]
HeIII_density = data[:, 9]

ne = data[:, 10]  # number density


# compute number density for all species

nHI = HI_density * density_units / (mh / mass_units)
# in part per cc
nHII = HII_density * density_units / (mh / mass_units)
# in part per cc
nHeI = HeI_density * density_units / (4 * (mh / mass_units))
# in part per cc
nHeII = HeII_density * density_units / (4 * (mh / mass_units))
# in part per cc
nHeIII = HeIII_density * density_units / (4 * (mh / mass_units))
# in part per cc
ne = ne * density_units / (mh / mass_units)
# in part per cc

n = nHI + nHII + nHeI + nHeII + nHeIII


fig, axs = pt.subplots(3, 1)
fig.set_size_inches(6, 6)


axs[0].plot(Time, Temperature, **plotkwargs)
#  axs[0].scatter(Time, Temperature)
axs[0].semilogy()
#axs[0].set_xlabel(r"$\rm{Time\,\,[Myr]}$")
axs[0].set_ylabel(r"$\rm{Temperature\,\,[K]}$")
axs[0].set_xscale("log")
axs[0].plot(
    [0.5e6, 0.5e6],
    [Temperature.min(), Temperature.max()],
    label="End of radiation",
    c="k",
    zorder=-1,
    lw=1,
)

# add iliev 2006 data
data_Iliev = np.loadtxt("data/IlievData_Tvst.txt")
axs[0].scatter(10**data_Iliev[:,0],10**data_Iliev[:,1],s=1,c="red",label="Mean solution (Iliev et al. 2006)")


axs[0].set_xticks([])
axs[0].legend()





axs[1].plot(Time, nHI/(nHI+nHII), **plotkwargs)
axs[1].loglog()
#axs[1].set_xlabel(r"$\rm{Time\,\,[yr]}$")
axs[1].set_ylabel(r"$\rm{nHI/(nHI+nHII)}$")

# add iliev 2006 data
data_Iliev = np.loadtxt("data/IlievData_nHvst.txt")
axs[1].scatter(10**data_Iliev[:,0],10**data_Iliev[:,1],s=1,c="red",label="Mean solution (Iliev et al. 2006)")

axs[1].set_xticks([])

#axs[1].legend()




axs[2].plot(Time, nHI, label=r"$\rm{HI}$", **plotkwargs)
axs[2].plot(Time, nHII, "--", label=r"$\rm{HII}$", **plotkwargs)
axs[2].plot(Time, nHeI, "-.", label=r"$\rm{HeI}$", **plotkwargs)
axs[2].plot(Time, nHeII, label=r"$\rm{HeII}$", **plotkwargs)
axs[2].plot(Time, nHeIII, label=r"$\rm{HeIII}$", **plotkwargs)
axs[2].plot(Time, ne, ":", label=r"$\rm{n_e}$", **plotkwargs)

axs[2].plot(Time, n, label=r"$\rm{Tot}$", alpha=1)

axs[2].set_xlabel(r"$\rm{Time\,\,[Myr]}$")
axs[2].set_ylabel(r"$\rm{Number\,\,Densities}$")
axs[2].set_xscale("log")
axs[2].plot([0.5e6, 0.5e6], [0.0, 1.0], c="k", zorder=-1, lw=1)


axs[2].legend()



#pt.show()
pt.savefig("rt_output.png", dpi=300)
